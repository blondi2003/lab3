﻿using System;

namespace lab3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y, z;
            x = Convert.ToInt32(Console.ReadLine());
            y = Convert.ToInt32(Console.ReadLine());
            z = Convert.ToInt32(Console.ReadLine());
            bool a = Convert.ToBoolean(x);
            bool b = Convert.ToBoolean(y);
            bool c = Convert.ToBoolean(z);

            bool f= !a | !b | b & !c;
            if (f)
            {
                Console.WriteLine(true);
            }
            else
            {
                Console.WriteLine(false);
            }

            Console.ReadKey();

        }
    }
}
