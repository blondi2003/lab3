﻿using System;

namespace lab._3._4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d;

            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());
            d = Convert.ToInt32(Console.ReadLine());

            if (a % 2 == 0 && b % 2 == 0 && c % 2 == 0 && d % 2 == 0)
            {
                Console.WriteLine("Поля одного цвета(черный)");
            }
            else if (a % 2 == 1 && b % 2 == 1 && c % 2 == 1 && d % 2 == 1)
            {
                Console.WriteLine("Поля одного цвета(черный)");
            }
            else if (a % 2 == 1 && b % 2 == 0 && c % 2 == 1 && d % 2 == 0)
            {
                Console.WriteLine("Поля одного цвета (белый)");
            }
            else if (a % 2 == 0 && b % 2 == 1 && c % 2 == 0 && d % 2 == 0)
            {
                Console.WriteLine("Поля одного цвета (белый)");
            }
            else
            {
                Console.WriteLine("Поля разных цветов");
            }

            Console.ReadKey();